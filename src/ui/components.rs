use gtk::{
    self, prelude::*,
    Align, Orientation,
    Label, Button, SpinButton,
    TreeView, TreeViewColumn, TreeViewGridLines, TreePath,
    CellRendererText, ListStore,
    Frame, ScrolledWindow, PolicyType,
    MessageDialog, DialogFlags, MessageType, ButtonsType,
    Window, WindowType,
    CssProvider,
};
use weekday::Date;
use ui::history::{History, HistoryItem};
use std::error::Error;

const INFO_TEXT: &str = "Weekday calculation for Gregorian dates based on the Doomsday rule.\r\n\r\n\
Dates are displayed using the ISO 8601 standard (YYYY-MM-DD).\r\n\r\n\
Note that the correctness until 9999-12-31 cannot be guaranteed because inaccuracies of the \
Gregorian calender might cause the addition of an extra leap day during this time period.";

/// Provides CSS to a `Widget`.
fn provide_css<W: WidgetExt>(widget: &W, css: &str) -> Result<(), gtk::Error> {
    let provider = CssProvider::new();
    provider.load_from_data(css.as_bytes())?;

    if let Some(style_context) = widget.get_style_context() {
        style_context.add_provider(&provider, gtk::STYLE_PROVIDER_PRIORITY_APPLICATION);
    }

    Ok(())
}

/// Shows a `MessageDialog` in a popup `Window`.
fn show_message_dialog(message_type: MessageType, title: &str, message: &str) {
    let dialog = MessageDialog::new(
        Some(&Window::new(WindowType::Popup)),
        DialogFlags::empty(),
        message_type,
        ButtonsType::Close,
        title
    );
    dialog.set_property_secondary_text(Some(message));

    dialog.run();
    dialog.destroy();
}

/// Sets up a `SpinButton` with a corresponding `Label`. Returns the `SpinButton` and an enclosing `gtk::Box`.
fn set_up_spinbutton_with_label(label_text: &str, min_value: f64, max_value: f64, step: f64,
                                initial_value: f64, max_length: i32) -> (SpinButton, gtk::Box) {
    let label = Label::new(label_text);
    provide_css(
        &label,
        ".label { font-size: 25px; text-decoration: underline; }"
    ).unwrap_or_default();

    let spinbutton = SpinButton::new_with_range(min_value, max_value, step);
    provide_css(
        &spinbutton,
        ".entry { font-size: 49px; border-style: solid; border-width: 1px; border-color: #666564; }"
    ).unwrap_or_default();
    spinbutton.set_digits(0);
    spinbutton.set_wrap(true);
    spinbutton.set_property_xalign(1.0);
    spinbutton.set_value(initial_value);
    spinbutton.set_max_length(max_length);

    let enclosing_box = gtk::Box::new(Orientation::Vertical, 10);
    enclosing_box.set_valign(Align::Center);
    enclosing_box.pack_start(&label, false, false, 0);
    enclosing_box.pack_start(&spinbutton, false, false, 0);

    (spinbutton, enclosing_box)
}

/// Sets up and returns a `TreeViewColumn`.
fn set_up_tree_view_column(title: &str, column_id: i32) -> TreeViewColumn {
    let cell_renderer = CellRendererText::new();
    cell_renderer.set_property_xalign(0.5);

    let column = TreeViewColumn::new();
    column.set_title(title);
    column.set_alignment(0.5);
    column.set_fixed_width(125);
    column.pack_start(&cell_renderer, false);
    column.add_attribute(&cell_renderer, "text", column_id);

    column
}

/// Sets up a scrollable `TreeView` with a `Frame`. Returns the `TreeView` and an enclosing `gtk::Box`.
fn set_up_tree_view(list_store: &ListStore) -> (TreeView, gtk::Box) {
    let tree_view = TreeView::new_with_model(list_store);
    tree_view.set_grid_lines(TreeViewGridLines::Vertical);
    tree_view.append_column(&set_up_tree_view_column("Date", 0));
    tree_view.append_column(&set_up_tree_view_column("Weekday", 1));

    let scrolled_window = ScrolledWindow::new(None, None);
    scrolled_window.set_policy(PolicyType::Automatic, PolicyType::Automatic);
    scrolled_window.set_min_content_height(181);
    scrolled_window.set_min_content_width(250);
    scrolled_window.add(&tree_view);

    let frame = Frame::new(None);
    provide_css(
        &frame,
        ".frame { border-style: solid; border-width: 1px; border-color: #666564; }"
    ).unwrap_or_default();
    frame.add(&scrolled_window);

    let enclosing_box = gtk::Box::new(Orientation::Vertical, 0);
    enclosing_box.set_valign(Align::Center);
    enclosing_box.pack_start(&frame, false, false, 0);

    (tree_view, enclosing_box)
}

/// Provides logic for the weekday calculation `Button`.
fn calc_button_connect_clicked(calc_button: &Button, year_sb: &SpinButton, month_sb: &SpinButton, day_sb: &SpinButton,
                               result_label: &Label, history: &History, tree_view: &TreeView) {
    let year_sb = year_sb.clone();
    let month_sb = month_sb.clone();
    let day_sb = day_sb.clone();
    let result_label = result_label.clone();
    let history = history.clone();
    let tree_view = tree_view.clone();

    calc_button.connect_clicked(move |_| {
        let try_date = Date::try_new(
            year_sb.get_value_as_int() as usize,
            month_sb.get_value_as_int() as usize,
            day_sb.get_value_as_int() as usize
        );
                    
        match try_date {
            Ok(date) => {
                let weekday = date.get_weekday();

                // Update the result Label, add a new item to the History, and scroll the TreeView up.
                result_label.set_text(&format!("{} is {}", date, weekday));
                history.add(HistoryItem::new(&format!("{}", date), &format!("{}", weekday)));
                tree_view.scroll_to_cell(Some(&TreePath::new_first()), None, false, 0.0, 0.0);
            },
            Err(err) => {
                show_message_dialog(MessageType::Error, "Error", err.description());
            },
        };
    });
}

/// Sets up `Button`s, and returns an enclosing `gtk::Box`.
fn set_up_buttons(year_sb: &SpinButton, month_sb: &SpinButton, day_sb: &SpinButton,
                  result_label: &Label, history: &History, tree_view: &TreeView) -> gtk::Box {
    let calc_button = Button::new_with_label("Calculate Weekday");
    let clear_button = Button::new_with_label("Clear History");
    let info_button = Button::new_with_label("Show Information");

    for &button in &[&calc_button, &clear_button, &info_button] {
        provide_css(
            button,
            ".button { font-size: 12px; border-style: solid; border-width: 1px; border-color: #666564; }"
        ).unwrap_or_default();
    }

    calc_button_connect_clicked(&calc_button, year_sb, month_sb, day_sb, result_label, history, tree_view);

    {
        let history = history.clone();

        clear_button.connect_clicked(move |_| {
            history.clear();
        });
    }

    info_button.connect_clicked(move |_| {
        show_message_dialog(MessageType::Info, "Info", INFO_TEXT);
    });

    let enclosing_box = gtk::Box::new(Orientation::Vertical, 20);
    enclosing_box.set_valign(Align::Center);
    enclosing_box.pack_start(&calc_button, false, false, 0);
    enclosing_box.pack_start(&clear_button, false, false, 0);
    enclosing_box.pack_start(&info_button, false, false, 0);

    enclosing_box
}

/// Stores all of the UI components in a container of type `gtk::Box`.
pub struct Components {
    container: gtk::Box,
}

impl Components {
    /// Creates a new set of `Components`.
    pub fn new(history: &History) -> Components {
        let (year_sb, year_box) = set_up_spinbutton_with_label("Year", 1582.0, 9999.0, 1.0, 1582.0, 4);
        let (month_sb, month_box) = set_up_spinbutton_with_label("Month", 1.0, 12.0, 1.0, 10.0, 2);
        let (day_sb, day_box) = set_up_spinbutton_with_label("Day", 1.0, 31.0, 1.0, 15.0, 2);

        let spinbuttons_box = gtk::Box::new(Orientation::Horizontal, 5);
        spinbuttons_box.set_halign(Align::Center);
        spinbuttons_box.pack_start(&year_box, false, false, 20);
        spinbuttons_box.pack_start(&month_box, false, false, 0);
        spinbuttons_box.pack_start(&day_box, false, false, 20);

        let result_label = Label::new(None);
        provide_css(
            &result_label,
            ".label { font-size: 22px; font-weight: bold; }"
        ).unwrap_or_default();

        let result_box = gtk::Box::new(Orientation::Horizontal, 0);
        result_box.set_halign(Align::Center);
        result_box.pack_start(&result_label, false, false, 0);

        let (tree_view, tree_view_box) = set_up_tree_view(history.get_list_store());
        let buttons_box = set_up_buttons(&year_sb, &month_sb, &day_sb, &result_label, history, &tree_view);

        let tree_view_and_buttons_box = gtk::Box::new(Orientation::Horizontal, 50);
        tree_view_and_buttons_box.set_halign(Align::Center);
        tree_view_and_buttons_box.pack_start(&tree_view_box, false, false, 0);
        tree_view_and_buttons_box.pack_start(&buttons_box, false, false, 0);

        let container = gtk::Box::new(Orientation::Vertical, 20);
        container.set_valign(Align::Center);
        container.pack_start(&spinbuttons_box, false, false, 20);
        container.pack_start(&result_box, false, false, 0);
        container.pack_start(&tree_view_and_buttons_box, false, false, 20);

        Components { container }
    }

    /// Returns a reference to the container.
    pub fn get_container(&self) -> &gtk::Box {
        &self.container
    }
}
