use std::{
    error::Error,
    fmt,
    collections::{HashMap, hash_map::Entry},
    cell::RefCell,
    rc::Rc,
    io::{self, prelude::*, BufReader},
    path::Path,
    fs::File,
    iter::FromIterator,
};
use gtk::{prelude::*, ListStore, TreeIter, TreePath, Type};
use ron;

/// An error that can occur during file access or (de)serialization.
#[derive(Debug)]
pub enum SaveLoadError {
    Io(io::Error),
    RonSer(ron::ser::Error),
    RonDe(ron::de::Error),
}

impl From<io::Error> for SaveLoadError {
    fn from(err: io::Error) -> Self {
        SaveLoadError::Io(err)
    }
}

impl From<ron::ser::Error> for SaveLoadError {
    fn from(err: ron::ser::Error) -> Self {
        SaveLoadError::RonSer(err)
    }
}

impl From<ron::de::Error> for SaveLoadError {
    fn from(err: ron::de::Error) -> Self {
        SaveLoadError::RonDe(err)
    }
}

impl fmt::Display for SaveLoadError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "SaveLoadError")
    }
}

impl Error for SaveLoadError {
    fn description(&self) -> &str {
        match *self {
            SaveLoadError::Io(ref err) => err.description(),
            SaveLoadError::RonSer(ref err) => err.description(),
            SaveLoadError::RonDe(ref err) => err.description(),
        }
    }
}

/// An item storing a date and its corresponding weekday as `String`s.
#[derive(Debug, Serialize, Deserialize)]
pub struct HistoryItem {
    date: String,
    weekday: String,
}

impl HistoryItem {
    /// Creates a new `HistoryItem`.
    pub fn new(date: &str, weekday: &str) -> HistoryItem {
        HistoryItem {
            date: date.to_string(),
            weekday: weekday.to_string(),
        }
    }
}

/// An iterator over a `History`.
#[derive(Debug)]
pub struct HistoryIter<'a> {
    list_store: &'a ListStore,
    current_tree_path: TreePath,
}

impl<'a> HistoryIter<'a> {
    /// Creates a new `HistoryIter`.
    fn new(list_store: &ListStore) -> HistoryIter {
        HistoryIter {
            list_store,
            current_tree_path: TreePath::new_first(),
        }
    }
}

impl<'a> Iterator for HistoryIter<'a> {
    type Item = HistoryItem;

    fn next(&mut self) -> Option<Self::Item> {
        // If the current TreePath refers to an existing row of 
        // the ListStore, retrieve the corresponding TreeIter.
        if let Some(tree_iter) = self.list_store.get_iter(&self.current_tree_path) {
            // Move the TreePath to the next row for the next step of the iteration.
            self.current_tree_path.next();

            // Create a new HistoryItem from the values provided by the TreeIter, and return it.
            if let (Some(date), Some(weekday)) = (self.list_store.get_value(&tree_iter, 0).get(),
                                                  self.list_store.get_value(&tree_iter, 1).get()) {
                return Some(HistoryItem::new(date, weekday));
            }
        }

        None
    }
}

/// A history storing dates and their corresponding weekdays in a `ListStore`.
#[derive(Debug)]
pub struct History {
    list_store: ListStore,
    map: Rc<RefCell<HashMap<String, TreeIter>>>,
}

impl History {
    /// Creates a new, empty `History`.
    pub fn new() -> History {
        History {
            list_store: ListStore::new(&[Type::String, Type::String]),
            map: Rc::new(RefCell::new(HashMap::new())),
        }
    }

    /// Tries to load a `History` from a file and return it.
    /// 
    /// Returns a `SaveLoadError` in case of failure.
    pub fn load_from_file<P: AsRef<Path>>(path: P) -> Result<History, SaveLoadError> {
        let file = File::open(path)?;
        let buf_reader = BufReader::new(file);
        let deserialized: Vec<HistoryItem> = ron::de::from_reader(buf_reader)?;

        Ok(deserialized.into_iter().collect())
    }

    /// Tries to save the `History` to a file.
    /// 
    /// Returns a `SaveLoadError` in case of failure.
    pub fn save_to_file<P: AsRef<Path>>(&self, path: P) -> Result<(), SaveLoadError> {
        let serialized = ron::ser::to_string_pretty(
            &self.iter().collect::<Vec<HistoryItem>>(),
            ron::ser::PrettyConfig::default()
        )?;

        let mut file = File::create(path)?;
        file.write_all(serialized.as_bytes())?;

        Ok(())
    }

    /// Inserts a `HistoryItem` into the `History` at the specified position.
    /// 
    /// Inserting an already stored item will move it to
    /// position 0 instead of creating a duplicate.
    fn insert(&self, item: HistoryItem, position: Option<u32>) {
        // Check whether the date is already stored.
        match self.map.borrow_mut().entry(item.date) {
            Entry::Occupied(occ) => {
                // If so, move the corresponding TreeIter to the beginning of the ListStore.
                self.list_store.move_after(occ.get(), None);
            },
            Entry::Vacant(vac) => {
                // Otherwise, insert the values into the ListStore.
                let tree_iter = self.list_store.insert_with_values(
                    position,
                    &[0, 1],
                    &[&Some(vac.key()), &Some(&item.weekday)]
                );

                // Store the corresponding TreeIter in the HashMap.
                vac.insert(tree_iter);
            },
        }
    }

    /// Adds a `HistoryItem` item to the `History`.
    /// 
    /// Adding an already stored item will move it to the top
    /// of the `History` instead of creating a duplicate.
    pub fn add(&self, item: HistoryItem) {
        self.insert(item, Some(0))
    }

    /// Clears the `History`.
    pub fn clear(&self) {
        self.list_store.clear();
        self.map.borrow_mut().clear();
    }

    /// Returns a reference to the `ListStore`.
    pub fn get_list_store(&self) -> &ListStore {
        &self.list_store
    }

    /// Returns an iterator over the `History`.
    pub fn iter(&self) -> HistoryIter {
        HistoryIter::new(&self.list_store)
    }
}

impl Clone for History {
    fn clone(&self) -> Self {
        History {
            list_store: self.list_store.clone(),
            map: Rc::clone(&self.map),
        }
    }
}

impl Default for History {
    fn default() -> Self {
        History::new()
    }
}

impl FromIterator<HistoryItem> for History {
    fn from_iter<I: IntoIterator<Item=HistoryItem>>(iter: I) -> Self {
        let history = History::new();

        for item in iter {
            history.insert(item, None);
        };

        history
    }
}
