mod history;
mod components;

use gtk::{prelude::*, Application, ApplicationWindow, WindowPosition, HeaderBar};
use self::history::History;
use self::components::Components;

/// Builds the UI, and sets up all of its logic.
pub fn build_ui(application: &Application) {
    let header_bar = HeaderBar::new();
    header_bar.set_title("Weekday Calculator");
    header_bar.set_show_close_button(true);

    let history = History::load_from_file("weekday_history.ron").unwrap_or_default();
    let components = Components::new(&history);

    let window = ApplicationWindow::new(application);
    window.set_position(WindowPosition::Center);
    window.set_default_size(775, 700);
    window.set_title("Weekday Calculator");
    window.set_titlebar(&header_bar);
    window.set_icon_from_file("weekday_icon.ico").unwrap_or_default();
    window.add(components.get_container());
    window.show_all();

    {
        let history = history.clone();

        window.connect_delete_event(move |wdw, _| {
            history.save_to_file("weekday_history.ron").unwrap_or_default();
            wdw.destroy();

            Inhibit(false)
        });
    }
}
