//! Weekday calculation for Gregorian dates based on the Doomsday rule.
//! 
//! Dates are displayed using the ISO 8601 standard (YYYY-MM-DD).
//! 
//! Note that the correctness for future dates cannot be guaranteed because inaccuracies
//! of the Gregorian calender might eventually cause the addition of an extra leap day.

use std::{fmt, error::Error, cmp::Ordering};

/// An error that results from trying to create an invalid or a non-Gregorian `Date`.
#[derive(Debug, PartialEq, Eq)]
pub enum DateError {
    InvalidDate,
    NonGregorianDate,
}

impl fmt::Display for DateError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "DateError")
    }
}

impl Error for DateError {
    fn description(&self) -> &str {
        match *self {
            DateError::InvalidDate => {
                "The given date is invalid."
            },
            DateError::NonGregorianDate => {
                "The given date is not Gregorian.\r\n\
                The Gregorian calender starts on 1582-10-15."
            },
        }
    }
}

/// A day of the week.
#[derive(Debug, PartialEq, Eq)]
pub enum Weekday {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
}

impl fmt::Display for Weekday {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

/// A type of year.
#[derive(Debug)]
enum YearType {
    StandardYear,
    LeapYear,
}

/// A valid Gregorian calender date.
/// 
/// The Gregorian calender starts on 1582-10-15.
#[derive(Debug)]
pub struct Date {
    year_type: YearType,
    year: usize,
    month: usize,
    day: usize,
}

impl Date {
    /// Tries to create and return a new `Date`.
    /// 
    /// Returns a `DateError` in case of failure.
    pub fn try_new(year: usize, month: usize, day: usize) -> Result<Date, DateError> {
        // Determine whether the year is a leap year or not.
        let year_type = match (year % 4 == 0, year % 100 == 0, year % 400 == 0) {
            (false, false, false) | (true, true, false) => YearType::StandardYear,
            (true, false, false) | (true, true, true) => YearType::LeapYear,
            _ => unreachable!(),
        };

        // Check input for validity.
        let is_valid = day >= 1 && match month {
            1 | 3 | 5 | 7 | 8 | 10 | 12 => { day <= 31 },
            4 | 6 | 9 | 11 => { day <= 30 },
            2 => match year_type {
                YearType::StandardYear => { day <= 28 },
                YearType::LeapYear => { day <= 29 },
            },
            _ => { false },
        };

        // Check input for 'Gregorianity'.
        let is_gregorian = match year.cmp(&1582).then(month.cmp(&10)).then(day.cmp(&15)) {
            Ordering::Less => false,
            _ => true,
        };

        // Return a value based on the results of the previous checks.
        // Validity takes priority over 'Gregorianity'.
        if !is_valid {
            Err(DateError::InvalidDate)
        } else if !is_gregorian {
            Err(DateError::NonGregorianDate)
        } else {
            Ok(Date { year_type, year, month, day })
        }
    }

    /// Calculates and returns the corresponding `Weekday`.
    pub fn get_weekday(&self) -> Weekday {
        // Determine the Doomsday of the given year.
        let year_doomsday = 8 + 5 * (self.year % 4)
                              + 4 * (self.year % 100)
                              + 6 * (self.year % 400);
        
        // Create an array including the first day of each month that is a Doomsday.
        let reference_days = match self.year_type {
            YearType::StandardYear => [3, 7, 7, 4, 2, 6, 4, 1, 5, 3, 7, 5],
            YearType::LeapYear     => [4, 1, 7, 4, 2, 6, 4, 1, 5, 3, 7, 5],
        };
        
        // Calculate the weekday based on the difference of the given day and the reference day.
        match (year_doomsday + self.day - reference_days[self.month - 1]) % 7 {
            0 => Weekday::Monday,
            1 => Weekday::Tuesday,
            2 => Weekday::Wednesday,
            3 => Weekday::Thursday,
            4 => Weekday::Friday,
            5 => Weekday::Saturday,
            6 => Weekday::Sunday,
            _ => unreachable!(),
        }
    }
}

impl fmt::Display for Date {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}-{:02}-{:02}", self.year, self.month, self.day)
    }
}
