extern crate gtk;
extern crate gio;
extern crate serde;
extern crate ron;
extern crate weekday;

#[macro_use]
extern crate serde_derive;

mod ui;

use gtk::Application;
use gio::{prelude::*, ApplicationFlags};
use std::env::args;

fn main() {
    let application = Application::new(
        "weekday.calculator",
        ApplicationFlags::empty()
    ).expect("Initialization failed.");

    application.connect_startup(move |app| {
        ui::build_ui(app);
    });

    application.connect_activate(|_| {});

    application.run(&args().collect::<Vec<_>>());
}
