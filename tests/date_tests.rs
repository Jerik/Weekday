extern crate weekday;

use weekday::{Date, DateError, Weekday};

/// Asserts that trying to create a invalid `Date` results in a `DateError`.
#[test]
fn invalid() {
    // Invalid and non-Gregorian date (invalidity takes priority).
    assert_eq!(Date::try_new(1582, 9, 31).unwrap_err(), DateError::InvalidDate);

    // February 29th in standard years.
    assert_eq!(Date::try_new(1900, 2, 29).unwrap_err(), DateError::InvalidDate);
    assert_eq!(Date::try_new(2018, 2, 29).unwrap_err(), DateError::InvalidDate);

    /// Nonsensical dates.
    assert_eq!(Date::try_new(2000, 0, 20).unwrap_err(), DateError::InvalidDate);
    assert_eq!(Date::try_new(2000, 13, 20).unwrap_err(), DateError::InvalidDate);
    assert_eq!(Date::try_new(2000, 5, 0).unwrap_err(), DateError::InvalidDate);
    assert_eq!(Date::try_new(2000, 5, 32).unwrap_err(), DateError::InvalidDate);

    /// 31st in 30-day-months.
    assert_eq!(Date::try_new(2003, 4, 31).unwrap_err(), DateError::InvalidDate);
    assert_eq!(Date::try_new(2003, 6, 31).unwrap_err(), DateError::InvalidDate);
    assert_eq!(Date::try_new(2003, 9, 31).unwrap_err(), DateError::InvalidDate);
    assert_eq!(Date::try_new(2003, 11, 31).unwrap_err(), DateError::InvalidDate);
}

/// Asserts that trying to create a non-Gregorian `Date` results in a `DateError`.
#[test]
fn non_gregorian() {
    // Valid dates before 1582-10-15.
    assert_eq!(Date::try_new(0, 1, 1).unwrap_err(), DateError::NonGregorianDate);
    assert_eq!(Date::try_new(1200, 2, 29).unwrap_err(), DateError::NonGregorianDate);

    // Valid dates before 1582-10-15 (edge cases).
    assert_eq!(Date::try_new(1582, 10, 14).unwrap_err(), DateError::NonGregorianDate);
    assert_eq!(Date::try_new(1582, 9, 15).unwrap_err(), DateError::NonGregorianDate);
    assert_eq!(Date::try_new(1581, 10, 15).unwrap_err(), DateError::NonGregorianDate);
}

/// Asserts that `Weekday`s are calculated correctly.
#[test]
fn weekdays() {
    // Dates at the beginning of the Gregorian calendar.
    assert_eq!(Date::try_new(1582, 10, 15).unwrap().get_weekday(), Weekday::Friday);
    assert_eq!(Date::try_new(1582, 11, 13).unwrap().get_weekday(), Weekday::Saturday);
    assert_eq!(Date::try_new(1583, 10, 14).unwrap().get_weekday(), Weekday::Friday);

    // February 29th in leap years.
    assert_eq!(Date::try_new(1904, 2, 29).unwrap().get_weekday(), Weekday::Monday);
    assert_eq!(Date::try_new(2000, 2, 29).unwrap().get_weekday(), Weekday::Tuesday);
    assert_eq!(Date::try_new(2016, 2, 29).unwrap().get_weekday(), Weekday::Monday);

    // Some interesting cases.
    assert_eq!(Date::try_new(1758, 12, 31).unwrap().get_weekday(), Weekday::Sunday);
    assert_eq!(Date::try_new(2035, 1, 3).unwrap().get_weekday(), Weekday::Wednesday);
    assert_eq!(Date::try_new(2035, 2, 28).unwrap().get_weekday(), Weekday::Wednesday);
    assert_eq!(Date::try_new(2035, 3, 1).unwrap().get_weekday(), Weekday::Thursday);
    assert_eq!(Date::try_new(2036, 1, 3).unwrap().get_weekday(), Weekday::Thursday);
    assert_eq!(Date::try_new(2036, 2, 28).unwrap().get_weekday(), Weekday::Thursday);
    assert_eq!(Date::try_new(2036, 3, 1).unwrap().get_weekday(), Weekday::Saturday);

    // Some random dates.
    assert_eq!(Date::try_new(1824, 5, 26).unwrap().get_weekday(), Weekday::Wednesday);
    assert_eq!(Date::try_new(2018, 4, 6).unwrap().get_weekday(), Weekday::Friday);
    assert_eq!(Date::try_new(2172, 12, 9).unwrap().get_weekday(), Weekday::Wednesday);
}

/// Asserts that `Date`s are displayed correctly.
#[test]
fn display() {
    assert_eq!(format!("{}", Date::try_new(1582, 10, 15).unwrap()), "1582-10-15");
    assert_eq!(format!("{}", Date::try_new(1824, 5, 26).unwrap()), "1824-05-26");
    assert_eq!(format!("{}", Date::try_new(2018, 4, 6).unwrap()), "2018-04-06");
    assert_eq!(format!("{}", Date::try_new(2172, 12, 9).unwrap()), "2172-12-09");
}
